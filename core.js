//Maximillon Tan
//10843981
//Intern @ GaloreTV

var init = angular.module('core', ['ngGrid']);
//---------------------------------------------------

init.controller('main', function main($scope) {

    //Global Variables
    $scope.is_form_complete = false;
    $scope.company_name = "";
    $scope.company_status = false;
    $scope.brand_status = true;
    $scope.brand_name = "";
    $scope.verify_status = true;

    //Source List Identifiers: (Company Name:[String]{Drop down}, Brand/s:[String[ARRAY]{Drop down}, Address:[String]{Read-Only Txt-Area},
    //... Contact Number:[String]{Read-Only Txt-Area}, Buyer:[String]{Read-Only Txt-Area}, Consignment Rate:[String]{Read-Only Txt-Area},
    //... Payment Terms:[String]{Read-Only Txt-Area}, Delivery Date:[String]{Read-Only Txt-Area}, Shipment Type:[String]{Drop down}

    $scope.myTable = [ //myData //Hard-Coded Data
        {name:"GaloreTV", address: "8th floor Century Tower", brands: ["Galore T.V. 1", "Galore T.V. 2"]},
        {name:"Wikipedia", address: "Internet Wiki", brands: ["Wikipedia 1", "Wikipedia 2"]},
        {name:"Meralco", address: "Quezon", brands: ["Meralco 1", "Meralco 2"]},
        {name:"PLDT", address: "Manila", brands: ["PLDT 1", "PLDT 2"]},
        {name:"Globe", address: "Mandaluyong", brands: ["Globe 1", "Globe 2"]},
        {name:"Smart", address: "Navotas", brands: ["Smart 1", "Smart 2"]},
        {name:"Talk and Text", address: "California", brands: ["Talk and Text 1", "Talk and Text 2"]},
        {name:"ABS-CBN", address: "Hong Kong", brands: ["ABS-CBN 1", "ABS-CBN 2"]},
        {name:"GMA", address: "Taipei", brands: ["GMA 1", "GMA 2"]},
        {name:"CNN", address: "Makati", brands: ["CNN 1", "CNN 2"]},
        {name:"BBC", address: "New York", brands: ["BBC 1", "BBC 2"]},
        {name:"Cartoon Network", address: "Toronto", brands: ["Cartoon Network 1", "Cartoon Network 2"]},
        {name:"Disney Channel", address: "Florida", brands: ["Disney Channel 1", "Disney Channel 2"]},
        {name:"Nickelodeon", address: "Muntinlupa", brands: ["Nickelodeon 1", "Nickelodeon 2"]},
        {name:"Person of Interest", address: "Alabang", brands: ["Person of Interest 1", "Person of Interest 2"]}
    ];//end of objects

    //Table Source List Identifiers: Supplier SKU:[String]{Txt-Area}, Product Name:[String]{Txt-Area}, Photo:[Link]{PNG/JPEG/JPG},
    //... Identifier Code:[String]{Read-Only Txt-Area}, Category:[String]{Txt-Area}, Sub-Category:[String]{Txt-Area},
    //... Age Group:[String]{Txt-Area}, Size:[String]{Txt-Area}, Color_Print:[String]{Txt-Area}, Gender:[String]{Txt-Area},
    //... Quantity:[Integer]{Txt-Area}, Cost:[Float]{Txt-Area}, SRP:[Float]{Txt-Area}, Measurement:[String]{Txt-Area},
    //... Features:[String]{Txt-Area}, Care_Instructions:[String]{Txt-Area}

    $scope.sourceList = {
        data: 'myTable',
        enableCellSelection: true,
        enableRowSelection: false,
        enableCellEdit: true,
        displaySelectionCheckbox: false,
        enablePinning: true,
        columnDefs: [{ field: "name", displayName: 'Name', width: 130, pinned: true, enableCellEdit: true},
            { field: "address", displayName: 'Address', width: 150 },
            { field: "brands", displayName: 'Brands', width: 240 }]
    };

    $scope.companySearch = function()
    {
        if ($scope.company_name!= "" && $scope.company_name != null){
            $scope.company_status = true;
            $scope.brand_status = false;
        }//end of if
        else if ($scope.company_name == "" || $scope.company_name == null){
            alert("Company's Name Field is Empty");
        }//end of if
    };//end of method

    $scope.brandSearch = function()
    {
        if ($scope.brand_name != "" && $scope.brand_name != null){
            $scope.brand_status = true;
            $scope.verify_status = false;
            //$scope.listTable();
        }//end of if
        else if ($scope.brand_name == "" || $scope.brand_name == null){
            alert("Brand's Name Field is Empty");
        }//end of if
    };//end of method

    $scope.verifyStatus = function()
    {
        $scope.verify_status = true;
        $scope.is_form_complete = true;
    };//end of method
});